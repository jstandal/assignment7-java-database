package com.jstandal.assignment7javadatabase.services;

import com.jstandal.assignment7javadatabase.models.Character;
import com.jstandal.assignment7javadatabase.repositories.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CharacterService {

    @Autowired
    private CharacterRepository repo;

    //Adds a character to database
    public Character createCharacter(Character character){
        return repo.save(character);
    }

    //Returns a list of all characters
    public List<Character> getAllCharacters(){
        return repo.findAll();
    }

    //Returns a character given by Id
    public Character getCharacterById(long id){
        return repo.findById(id).orElse(null);
    }

    //Returns a character given by name
    public Character getCharacterByName(String name){
        return repo.findByFullName(name);
    }

    //Deletes a character
    public void deleteCharacterById(long id){
        repo.deleteById(id);
    }

    //Update character
    public Character updateCharacter(Character character){
        return repo.save(character);
    }



}
