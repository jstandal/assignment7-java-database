package com.jstandal.assignment7javadatabase.services;

import com.jstandal.assignment7javadatabase.models.Franchise;
import com.jstandal.assignment7javadatabase.repositories.FranchiseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FranchiseService {

    @Autowired
    private FranchiseRepository franchiseRepository;

    //CREATE
    public void createFranchise(Franchise franchise){
        franchiseRepository.save(franchise);
    }

    //READ
    public Franchise getFranchiseById(long id){
        return franchiseRepository.getById(id);
    }

    public List<Franchise> getAllFranchises(){
        return franchiseRepository.findAll();
    }

    //UPDATE
    public void updateFranchise(Franchise franchise){
        franchiseRepository.save(franchise);
    }

    //DELETE
    public void deleteFranchiseById(long id){
        franchiseRepository.deleteById(id);
    }
}
