package com.jstandal.assignment7javadatabase.services;

import com.jstandal.assignment7javadatabase.models.Character;
import com.jstandal.assignment7javadatabase.models.Movie;
import com.jstandal.assignment7javadatabase.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieService {

    @Autowired
    private MovieRepository movieRepository;

    //CREATE
    public void createMovie(Movie movie){
        movieRepository.save(movie);
    }

    //READ
    public Movie getMovieById(long id){
        return movieRepository.getById(id);
    }

    public List<Movie> getAllMovies(){
        return movieRepository.findAll();
    }

    //UPDATE
    public void updateMovie(Movie movie){
        movieRepository.save(movie);
    }

    //DELETE
    public void deleteMovieById(long id){
        movieRepository.deleteById(id);
    }

    //Get all characters in movie by id
    public List<Character> getCharactersByMovieId(long id) {


    }

}
