package com.jstandal.assignment7javadatabase.controllers;

import com.jstandal.assignment7javadatabase.models.Franchise;
import com.jstandal.assignment7javadatabase.services.FranchiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FranchiseController {

    @Autowired
    private FranchiseService service;

    //Returns list of all franchises
    @GetMapping
    public ResponseEntity<List<Franchise>> getAllFranchises(){
        List<Franchise> franchises = service.getAllFranchises();
        return new ResponseEntity<>(franchises, HttpStatus.OK);
    }

    //Returns franchise by id
    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchiseById(@PathVariable long id){
        Franchise franchise = service.getFranchiseById(id);
        return new ResponseEntity<>(franchise, HttpStatus.OK);
    }

    //Creates a franchise
    @PostMapping
    public ResponseEntity<String> createFranchise(@RequestBody Franchise franchise){
        service.createFranchise(franchise);
        return new ResponseEntity<>("Franchise created.", HttpStatus.CREATED);
    }

    //Updates a franchise
    @PutMapping
    public ResponseEntity<String> updateFranchise(@RequestBody Franchise franchise){
        service.updateFranchise(franchise);
        return new ResponseEntity<>("Franchise updated.", HttpStatus.CREATED);
    }

    //Deletes a franchise by id
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteFranchise(@PathVariable long id){
        service.deleteFranchiseById(id);
        return new ResponseEntity<>("Franchise deleted.", HttpStatus.GONE);
    }


}
