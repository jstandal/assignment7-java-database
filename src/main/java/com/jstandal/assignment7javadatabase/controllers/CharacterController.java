package com.jstandal.assignment7javadatabase.controllers;

import com.jstandal.assignment7javadatabase.models.Character;
import com.jstandal.assignment7javadatabase.services.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/characters")
public class CharacterController {

    @Autowired
    private CharacterService service;

    //Returns a list of all characters
    @GetMapping
    public ResponseEntity<List<Character>> getAllCharacters(){
        List<Character> characters = service.getAllCharacters();
        return new ResponseEntity<>(characters, HttpStatus.OK);
    }

    //Gets a character by id
    @GetMapping("/{id}")
    public ResponseEntity<Character> getCharacterById(@PathVariable long id){
        Character character = service.getCharacterById(id);
        return new ResponseEntity<>(character,HttpStatus.OK);
    }

    //Creates a new character
    @PostMapping
    public ResponseEntity<String> createCharacter(@RequestBody Character character){
        service.createCharacter(character);
        return new ResponseEntity<>("Character created.", HttpStatus.CREATED);
    }

    //Updates a character
    @PutMapping
    public ResponseEntity<String> updateCharacter(@RequestBody Character character){
        service.updateCharacter(character);
        return new ResponseEntity<>("Character updated.", HttpStatus.OK);
    }

    //Deletes a character
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCharacterById(@PathVariable long id){
        service.deleteCharacterById(id);
        return new ResponseEntity<>("Character deleted.", HttpStatus.GONE);
    }



}
