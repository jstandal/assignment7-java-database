package com.jstandal.assignment7javadatabase.controllers;

import com.jstandal.assignment7javadatabase.models.Movie;
import com.jstandal.assignment7javadatabase.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MovieController {

    @Autowired
    private MovieService service;


    //Gets all movies
    @GetMapping
    public ResponseEntity<List<Movie>> getAllMovies(){
        List<Movie> movies = service.getAllMovies();
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }

    //Gets movie by id
    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovieById(@PathVariable long id){
        Movie movie = service.getMovieById(id);
        return new ResponseEntity<>(movie, HttpStatus.OK);
    }

    //Creates a new movie
    @PostMapping
    public ResponseEntity<String> createMovie(@RequestBody Movie movie){
        service.createMovie(movie);
        return new ResponseEntity<>("Movie created.", HttpStatus.CREATED);
    }

    //Updates a movie
    @PutMapping
    public ResponseEntity<String> updateMovie(@RequestBody Movie movie){
        service.updateMovie(movie);
        return new ResponseEntity<>("Movie updated.", HttpStatus.CREATED);
    }

    //Deletes a movie
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteMovie(@PathVariable long id){
        service.deleteMovieById(id);
        return new ResponseEntity<>("Movie deleted.", HttpStatus.GONE);
    }


}
