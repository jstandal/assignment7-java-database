package com.jstandal.assignment7javadatabase.models;

import javax.persistence.*;

@Entity
@Table(name="Character")
public class Character {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "full_name")
    private String fullName;

    @Column(name="alias")
    private String alias;

    @Column(name="gender")
    private String gender;

    @Column(name="picture")
    private String picture;

    @ManyToOne
    @JoinColumn(name="movie_id")
    private Movie movie;

    public Long getId() {
        return id;
    }

    public String getFullName(){
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public String getAlias() {
        return alias;
    }

    public String getGender() {
        return gender;
    }

    public String getPicture() {
        return picture;
    }

    public Movie getMovie() {
        return movie;
    }
}
