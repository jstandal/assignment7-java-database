package com.jstandal.assignment7javadatabase.models;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name="Movie")
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="movie_title")
    private String movieTitle;

    @Column(name="genre")
    private String genre;

    @Column(name="release_year")
    private Date releaseYear;

    @Column(name="director")
    private String director;

    @Column(name="picture")
    private String picture;

    @Column(name="trailer")
    private String trailer;

    @OneToMany(mappedBy = "movie")
    private Set<Character> characters;

    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;

}
