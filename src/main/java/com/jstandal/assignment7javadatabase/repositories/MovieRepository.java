package com.jstandal.assignment7javadatabase.repositories;

import com.jstandal.assignment7javadatabase.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
}
