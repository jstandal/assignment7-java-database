package com.jstandal.assignment7javadatabase.repositories;

import com.jstandal.assignment7javadatabase.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterRepository extends JpaRepository<Character, Long> {
    Character findByFullName(String name);
}
