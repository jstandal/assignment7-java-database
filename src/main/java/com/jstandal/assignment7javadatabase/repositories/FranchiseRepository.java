package com.jstandal.assignment7javadatabase.repositories;

import com.jstandal.assignment7javadatabase.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Long> {
}
