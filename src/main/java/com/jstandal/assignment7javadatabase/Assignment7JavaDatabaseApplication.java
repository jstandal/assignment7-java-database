package com.jstandal.assignment7javadatabase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assignment7JavaDatabaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(Assignment7JavaDatabaseApplication.class, args);
	}

}
